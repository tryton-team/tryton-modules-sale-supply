Metadata-Version: 2.1
Name: trytond_sale_supply
Version: 7.0.6
Summary: Tryton module for sale supply
Home-page: http://www.tryton.org/
Download-URL: http://downloads.tryton.org/7.0/
Author: Tryton
Author-email: foundation@tryton.org
License: GPL-3
Project-URL: Bug Tracker, https://bugs.tryton.org/
Project-URL: Documentation, https://docs.tryton.org/
Project-URL: Forum, https://www.tryton.org/forum
Project-URL: Source Code, https://code.tryton.org/tryton
Keywords: tryton sale supply
Classifier: Development Status :: 5 - Production/Stable
Classifier: Environment :: Plugins
Classifier: Framework :: Tryton
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: Financial and Insurance Industry
Classifier: Intended Audience :: Legal Industry
Classifier: License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)
Classifier: Natural Language :: Bulgarian
Classifier: Natural Language :: Catalan
Classifier: Natural Language :: Chinese (Simplified)
Classifier: Natural Language :: Czech
Classifier: Natural Language :: Dutch
Classifier: Natural Language :: English
Classifier: Natural Language :: Finnish
Classifier: Natural Language :: French
Classifier: Natural Language :: German
Classifier: Natural Language :: Hungarian
Classifier: Natural Language :: Indonesian
Classifier: Natural Language :: Italian
Classifier: Natural Language :: Persian
Classifier: Natural Language :: Polish
Classifier: Natural Language :: Portuguese (Brazilian)
Classifier: Natural Language :: Romanian
Classifier: Natural Language :: Russian
Classifier: Natural Language :: Slovenian
Classifier: Natural Language :: Spanish
Classifier: Natural Language :: Turkish
Classifier: Natural Language :: Ukrainian
Classifier: Operating System :: OS Independent
Classifier: Programming Language :: Python :: 3
Classifier: Programming Language :: Python :: 3.8
Classifier: Programming Language :: Python :: 3.9
Classifier: Programming Language :: Python :: 3.10
Classifier: Programming Language :: Python :: 3.11
Classifier: Programming Language :: Python :: 3.12
Classifier: Programming Language :: Python :: Implementation :: CPython
Classifier: Topic :: Office/Business
Classifier: Topic :: Office/Business :: Financial :: Accounting
Requires-Python: >=3.8
License-File: LICENSE
Requires-Dist: trytond_purchase<7.1,>=7.0
Requires-Dist: trytond_purchase_request<7.1,>=7.0
Requires-Dist: trytond_sale<7.1,>=7.0
Requires-Dist: trytond_stock<7.1,>=7.0
Requires-Dist: trytond<7.1,>=7.0
Provides-Extra: test
Requires-Dist: proteus<7.1,>=7.0; extra == "test"
Requires-Dist: trytond_stock_supply<7.1,>=7.0; extra == "test"

Sale Supply Module
##################

The Sale Supply module adds a "supply on sale option" to purchasable products.
If selected, it will generate a purchase request for each sale line of this
product depending of the option and the stock levels.
Once the purchased products are received they are assigned on the customer
shipments.
If the purchase is cancelled the sale goes back to the default supply method.

.. warning::
    If the shiment method is *On Invoice Paid*, the purchase request will be
    created only when all the invoice lines are paid
..
